﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Exemplo Framework Rest(c# com suporte ao .net core)</title>
	<link rel="stylesheet" href="Assets/css/bootstrap.min.css">

	<style media="screen">
		.extension-message {
			padding-top: 55px;
			padding-bottom: 55px;
		}
		.btn-extension-install {
			margin-bottom: 55px;
		}
	</style>
</head>
<body>
	<form id="form1" runat="server"> 
		
		<div class="container">
			<header class="header clearfix">
				<h2 class="text-muted"><img src="Assets/imgs/marca-signer-150dpi.png" style="width: 40px;" /> Exemplo Extensão para Assinatura Digital</h2>
			</header>
			<div id="extensao-instalada">
				<div class="row">
					<h3>1º Passo:</h3>
					<p>Selecione o certificado que deseja utilizar para assinar.</p>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-6">
						<select class="form-control"  id="select-certificado-list" onchange="BryApiModule.fillCertificateDataForm();"></select>
					</div>
					<a class="btn btn-default" href="#" onclick="BryApiModule.listCertificates()"><span class="glyphicon glyphicon-refresh"></span></a>
				</div>
				<div class="row">
					<h4>Dados do certificado selecionado:</h4>
				</div>
				<div class="row">
					<div class="row">
						<div class="form-group">
							<label class="col-md-4 control-label">Nome: </label>
							<div class="col-md-6">
								<input type="text" class="form-control" readonly value="Nome" id="input-nome">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<label class="col-md-4 control-label">Emissor: </label>
							<div class="col-md-6">
								<input type="text" class="form-control" readonly value="Emissor" id="input-emissor">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<label class="col-md-4 control-label">Data de validade: </label>
							<div class="col-md-6">
								<input type="text" class="form-control" readonly value="Data de validade" id="input-data-validade">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<label class="col-md-4 control-label">Tipo de certificado: </label>
							<div class="col-md-6">
								<input type="text" class="form-control" readonly value="Tipo de certificado" id="input-tipo">
							</div>
						</div>
					</div>
					<div class="row">
						Certificado Dados: <br/>
						<asp:TextBox ID="TextBoxDadosCertificado" TextMode="MultiLine" runat="server" Height="60px" Width="1000px"></asp:TextBox>
					</div>
				<div class="row">
					<h3>2º Passo:</h3>
					<p>Clique no botão baixo para gerar a assinatura.</p>
				</div>
				<div class="row">
					<div class="col-md-offset-4 col-md-4" style="text-align: center;">
						<asp:Button class="btn btn-lg btn-primary" ID="Button1" runat="server" OnClick="Inicializar_Click" Text="Inicializar Assinatura (Framework)" />
					</div>
				</div>

				<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
				<asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional" RenderMode="Inline">
					<ContentTemplate>

						<div style="padding-bottom: 5px"></div>
						<div class="row">
							Saída Serviço (Inicializar Assinatura)<br/>
							<asp:TextBox ID="TextBoxSaidaInicializar" TextMode="MultiLine" runat="server" Height="60px" Width="1000px"></asp:TextBox>
						</div>
						<div class="row">
							Entrada Extensão<br/>
							<asp:TextBox ID="TextBoxEntradaExtensao" TextMode="MultiLine" runat="server" Height="60px" Width="1000px"></asp:TextBox>
						</div>
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="Button1" />
					</Triggers>
				</asp:UpdatePanel>
				<div style="padding-bottom: 10px"></div>
				<div class="row">
					<div class="col-md-offset-4 col-md-4" style="text-align: center;">
						<a href="#" class="btn btn-lg btn-primary" onclick="BryApiModule.sign(); return false;">Assinar os dados com a Extensão (Client Side)</a>
					</div>
				</div>
				<div class="row">
					Saída Extensão<br/>
					<asp:TextBox ID="TextBoxSaidaExtensao" TextMode="MultiLine" runat="server" Height="60px" Width="1000px"></asp:TextBox>
				</div>
				<div style="padding-bottom: 10px"></div>
				<div class="row">
					<div class="col-md-offset-4 col-md-4" style="text-align: center;">
						<asp:Button class="btn btn-lg btn-primary" ID="Button3" runat="server" OnClick="Finalize_Click" Text="Finalizar Assinatura (Framework)" />
					</div>
				</div>
				<asp:UpdatePanel ID="UpdatePanel3" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional" RenderMode="Inline">
					<ContentTemplate>
						<div class="row">
							Saída Serviço (Finalizar Assinatura)<br/>
							<asp:TextBox ID="TextBoxFinalizar" TextMode="MultiLine" runat="server" Height="60px" Width="1000px"></asp:TextBox>
						</div>
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="Button3" />
					</Triggers>
				</asp:UpdatePanel>
				<div class="row">
					<p>
						<div class="alert alert-success" role="alert" id="success-message" style="display: none;">
							<a type="button" class="close" aria-label="Close" onclick="$('#success-message').hide()"><span aria-hidden="true">&times;</span></a>
							Assinatura concluída com sucesso.
						</div>
					</p>
					<p>
						<div class="alert alert-warning" role="alert" id="error-message" style="display: none;">
							<a type="button" class="close" aria-label="Close" onclick="$('#error-message').hide()"><span aria-hidden="true">&times;</span></a>
							Ocorreu algum erro. Mensagem: <span id="error-message-text"></span><br /> Informe este código para o suporte: <span class="badge" id="codigo-de-erro"></span>.
						</div>
					</p>
					<p>
						<div class="alert alert-warning" role="alert" id="update_windows" style="display: none; text-align: center">
							<a type="button" class="close" aria-label="Close" onclick="$('#update_windows').hide()"><span aria-hidden="true">&times;</span></a>
							O Módulo da Extensão precisa ser atualizado<br /><a onclick="BryApiModule.downloadNativeModuleWindows();" class="btn btn-success" >Atualizar</a>
						</div>
						<div class="alert alert-warning" role="alert" id="update_linux" style="display: none; text-align: center">
							<a type="button" class="close" aria-label="Close" onclick="$('#update_linux').hide()"><span aria-hidden="true">&times;</span></a>
							O Módulo da Extensão precisa ser atualizado<br /><a onclick="BryApiModule.downloadNativeModuleDeb();" class="btn btn-success" >DEB</a> <a onclick="BryApiModule.downloadNativeModuleRpm();" class="btn btn-success" >RPM</a>
						</div>
						<div class="alert alert-warning" role="alert" id="update_macos" style="display: none; text-align: center">
							<a type="button" class="close" aria-label="Close" onclick="$('#update_macos').hide()"><span aria-hidden="true">&times;</span></a>
							O Módulo da Extensão precisa ser atualizado<br /><a onclick="BryApiModule.downloadNativeModuleMacOS();" class="btn btn-success" >Atualizar</a>
						</div>
					</p>
				</div>
			</div> <!-- end extensao-instalada -->           
		</div>
	</form>
	 <div id="extensao-nao-instalada">
			<div class="container" style="text-align: center;">
				<div id="chrome-browser" class="row">
				</br>
					<p>Exemplo Instalação Redirecionando para a Chrome WebStore </p>
					<p>Segue abaixo um exemplo de passo a passo para instruir o usuário na instalação da extensão</p>

					<p><strong>1º Passo -</strong> Clique no botão abaixo para acessar a Extensão no Chrome WebStore</strong></p>
					<a  onclick="BryApiModule.installExtension(); return false;" class="btn btn-lg btn-primary btn-extension-install">Instalar Extensão via Chrome WebStore!</a>
					<p><strong>2º Passo -</strong> Clique no botão <strong>USAR NO CHROME</strong></p>
					<img alt="Download" src="Assets/imgs/use_on_chrome_button.jpg">					
					<p><br/><br/><strong>3º Passo -</strong> Você deve retornar para esta página que ela será atualizada.</p>           
				</div>
				<div id="firefox-browser" class="row">
					<h3 class="extension-message">Detectamos que a Extensão para Assinatura Digital não está instalada.</h3>
					<p > Exemplo Instalação Inline. Para esta opção é necessário realizar o registro da extensão.</p>
					<a onclick="BryApiModule.installExtension(); return false;" class="btn btn-lg btn-primary btn-extension-install">Instalar!</a><br/>
				</div>
				<div id="edge-browser">
					<h3 class="extension-message">Lamentamos, mas uma versão da extensão só estará disponível para o seu navegador na próxima atualização!</h3>
				</div>
				<div id="opera-browser">
					<h3 class="extension-message">Lamentamos, mas uma versão da extensão só estará disponível para o seu navegador na próxima atualização!</h3>
				</div>
				<div id="safari-browser">
					<br/><h3>Detectamos que a Extensão para Assinatura Digital não está instalada</h3><br/>
					<input type="image" width="250" weight="350" onclick="BryApiModule.installExtension(); return false;" src="Assets/imgs/baixar.png" /><br/><br/>
					<div class="centered">
					 <h4>Após a instalação é necessário habilitar a extensão nas preferências do Safari.</h4>
					 <p><b>1º Abra o aplicativo "BRy Assinatura Digital" instalado</b><br/> </p>
					 <strong>2º Dentro do aplicativo, clique em habilitar a extensão nas preferências do Safari</strong><br/>
					 
					 <img  src="Assets/imgs/app.png"/><br/>
					 <br/><strong>3º Marque a extensão como habilitada </strong><br/><br/>
					 <img src="Assets/imgs/ext.png"/><br/>
					 <p>Caso a opção para habilitar a extensão não apareça nas preferências do Safari, encerre o navagador, e repita o passo 2.
					 <br/><br/><strong>4º Após ativar a extensão, basta recarregar a página. </strong></p></div>
					 <a onclick="window.location.reload();" class="btn btn-lg btn-primary btn-extension-install">Recarregar página</a><br/>
				</div>
				<div id="ie-browser">
					<h3 class="extension-message">Lamentamos, mas uma versão da extensão só estará disponível para o seu navegador na próxima atualização!</h3>
				</div>
				<div id="unknown-browser">
					<h3 class="extension-message">Não foi possível identificar o seu browser!</h3>
				</div>
			</div>
		</div> <!-- end extensao-nao-instalada -->
	<footer>
		<div class="container">
			<hr>
			<p>&copy; 2020 BRy Tecnologia S.A</p>
		</div>
	</footer>
	<script src="https://www.bry.com.br/downloads/extension/v2/api/v1/extension-api.js"></script>
	<script src="Assets/js/jquery-3.2.1.min.js"></script>
	<script src="Assets/js/bootstrap.min.js"></script>
	<script src="Assets/js/script-customizavel.js"></script>
</body>
</html>
